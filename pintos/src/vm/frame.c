#include "vm/frame.h"
#include "threads/malloc.h"
#include "threads/thread.h"
#include "userprog/pagedir.h"
#include "vm/swap.h"
#include "userprog/syscall.h"

void frame_init(void)
{
	lock_init(&frame_lock);
	list_init(&frame_table);
}


void * get_frame(struct vm_page *page, enum palloc_flags flags, bool evicting)
{
	void *addr = palloc_get_page(flags);

	//printf("get_addr : 0x%x\n", addr);

	//printf("page : 0x%x, ", page->addr);

	if(addr == NULL)
	{
		//printf("start evict\n");
/*
		if(evicting)
			PANIC("Cannot Evict Frame!");*/

		addr = frame_evict(page, flags);

		//printf("evict--- : 0x%x\n", addr);

		if(addr == NULL)
			PANIC("Cannot Evict Frame!");
	}


	//printf("get_addr : 0x%x\n", addr);

	if(!evicting)
	{

		struct vm_frame *fr = malloc (sizeof (struct vm_frame));

		//printf("file : 0x%x\n", file_get_inode(page->file));

		if(fr == NULL)
			return NULL;

		frame_add_table(fr, page, addr);
	}

	//printf("get_addr--- : 0x%x\n", addr);

	return addr;
}

void free_frame(void *addr)		// can modify. i want to change it's parameter struct vm_frame
{
	lock_acquire(&frame_lock);

	struct vm_frame *fr = find_frame(addr);

	struct thread *cur = get_thread(fr->tid);

	if(fr != NULL)
	{
		//printf("frame free process\n");
		list_remove(&fr->list_elem);

		if(fr->page->loaded)
			pagedir_clear_page(cur->pagedir, fr->page->addr);

		fr->page->kpage = NULL;
		fr->page->loaded = false;

		free(fr);

		//printf("free addr : 0x%x\n", addr);
		palloc_free_page(addr);

	}



	//printf("free accept : 0x%x\n", addr);

	lock_release(&frame_lock);
}

struct vm_frame * find_frame(void *addr)
{
	struct list_elem *e;

	for(e = list_begin(&frame_table); e != list_end(&frame_table);
		e = list_next(e))
	{
		struct vm_frame *fr = list_entry(e, struct vm_frame, list_elem);

		if(fr->addr == addr)
			return fr;
	}

	return NULL;
}

void frame_add_table(struct vm_frame *frame, struct vm_page *page, void *addr)
{
	frame->addr = addr;
	frame->page = page;
	frame->tid = thread_current()->tid;

	//printf("pa : 0x%x, set fr->pa : 0x%x\n", page->addr, frame->page->addr);

	lock_acquire(&frame_lock);
	list_push_back(&frame_table, &frame->list_elem);
	lock_release(&frame_lock);

}

bool grow_stack (void *addr)
{
	struct vm_page *p;

	//valid_address(addr);

	if(!get_zero_page_table(addr, true))
		return false;

	p = page_lookup(addr);

	//printf("addr_p : 0x%x\n", p->addr);

	if(load_page(p))
	{
		return true;
	}

	return false;
}

void* frame_evict (struct vm_page *p, enum palloc_flags flag)
{
	lock_acquire(&frame_lock);

	struct list_elem *e = list_begin(&frame_table);

	struct list_elem *temp = list_prev(e);

	bool success = false;


	//printf("evict start\n");

	while(1)
	{
		struct vm_frame *fr = list_entry(e, struct vm_frame, list_elem);

		struct thread *cur = get_thread(fr->tid);

		if(pagedir_is_accessed(cur->pagedir, fr->page->addr)){
			pagedir_set_accessed(cur->pagedir, fr->page->addr, false);
		}else
		{
			if(pagedir_is_dirty(cur->pagedir, fr->page->addr)
				|| fr->page->type == SWAP)
			{
				fr->page->type = SWAP;
				fr->page->swap_index = swap_get(fr->addr);
				
			}
			if(fr->page->type == MMAP)
			{
				file_write_at(fr->page->file, fr->page->addr, fr->page->read_bytes,
								fr->page->offset);
			}

			fr->page->kpage = NULL;

			fr->page->loaded = false;

			//printf("fr.addr : 0x%x, p.addr : 0x%x\n", fr->page->addr, p->addr);

			lock_release(&frame_lock);

			free_frame(fr->addr);

			//printf("done evict\n");

			return get_frame(p, flag, 1);
		}

		e = list_next(e);
		if(e == list_end(&frame_table))
			e = list_begin(&frame_table);

		 /*if(e == temp)
		 	return NULL;*/
	}
/*
	while(!false)
	{
		struct vm_frame *fr = list_entry(e, struct vm_frame, list_elem);

		struct thread *cur = get_thread(fr->tid);

		if(!pagedir_is_accessed(cur->pagedir, fr->page->addr)
			&& !pagedir_is_dirty(cur->pagedir, fr->page->addr))

	}*/

	lock_release(&frame_lock);


}