#include "vm/swap.h"
#include "lib/debug.h"
#include "threads/vaddr.h"

#define BLOCK_SIZE_PER_PAGE (PGSIZE / BLOCK_SECTOR_SIZE)

void swap_init(void)
{
	swap_block = block_get_role (BLOCK_SWAP);

	if(swap_block == NULL)
		PANIC("SWAP BLOCK INIT FAILED");

	lock_init(&swap_lock);

	swap_map = bitmap_create(block_size(swap_block));

	bitmap_set_all(swap_map, true);
}

size_t swap_get (void *addr)	// write out swap table
{
	lock_acquire(&swap_lock);

	//size_t swap_index = bitmap_scan_and_flip (swap_map, 0 , 1, false);

	size_t swap_index = bitmap_scan(swap_map, 0, 1, true);

	if(swap_index == BITMAP_ERROR)
		PANIC("swap_index is BITMAP_ERROR");

	size_t i;

	for(i = 0; i < BLOCK_SIZE_PER_PAGE; i++)
	{
		block_write(swap_block, (swap_index * BLOCK_SIZE_PER_PAGE) + i,
			addr + (i * BLOCK_SIZE_PER_PAGE));
	}

	bitmap_set(swap_map, swap_index, false);

	lock_release(&swap_lock);

	//printf("get swap page : %d\n", swap_index);

	return swap_index;
}

void swap_load(size_t swap_index, void * addr)		//read in swap table
{
	lock_acquire(&swap_lock);

	if(!bitmap_test(swap_map, swap_index))
	{
		PANIC("Swap free block or empty swap page");
	}

	size_t i;
	for(i = 0; i < BLOCK_SIZE_PER_PAGE; i++)
	{
		block_read(swap_block, (swap_index * BLOCK_SIZE_PER_PAGE) + i,
			addr + (i * BLOCK_SIZE_PER_PAGE));
	}

	bitmap_set(swap_map, swap_index, true);

	lock_release(&swap_lock);
}

void swap_free(size_t swap_index)
{
	lock_acquire(&swap_lock);

	size_t i;
	for(i = 0; i < BLOCK_SIZE_PER_PAGE; i++)
	{
		bitmap_reset(swap_map, (swap_index * BLOCK_SIZE_PER_PAGE) + i);
	}

	lock_release(&swap_lock);
}