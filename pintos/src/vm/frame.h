#ifndef VM_FRAME_H
#define VM_FRAME_H

#include "vm/page.h"
#include "lib/kernel/list.h"
#include "threads/synch.h"
#include "threads/palloc.h"
#include "threads/thread.h"

//#include "userprog/pagedir.h"

struct lock frame_lock;
struct list frame_table;

struct vm_frame
{
	void *addr;

	tid_t tid;

	struct vm_page *page;

	struct list_elem list_elem;
};

void frame_init(void);
void *get_frame(struct vm_page *, enum palloc_flags, bool);
void free_frame(void *);
struct vm_frame * find_frame(void *);
void frame_add_table(struct vm_frame *, struct vm_page *, void *);

bool grow_stack(void *);

/* evict */

void * frame_evict (struct vm_page *, enum palloc_flags flags);

#endif /* VM_FRAME_H */