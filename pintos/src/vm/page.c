#include "vm/page.h"

#include <stdio.h>

#include "threads/thread.h"

#include "threads/malloc.h"
#include "threads/palloc.h"
#include "lib/string.h"
#include "userprog/pagedir.h"

//#include "userprog/syscall.h"

#include "vm/frame.h"
#include "vm/swap.h"
#include "userprog/process.h"

#include "threads/vaddr.h"



void page_init (void)
{
	lock_init(&page_lock);
}

void free_page (struct vm_page *p)
{
	if(p == NULL)
	{
		PANIC("No page delete!");
	}

	if(p->kpage != NULL)
		free_frame(p->kpage);

	if(p->loaded)
	{
		pagedir_clear_page(thread_current()->pagedir, p->addr);
	}

	file_close(p->file);

	hash_delete(&thread_current()->page_table, &p->hash_elem);
	free(p);
}

bool get_file_page_table(struct file *file, off_t ofs, uint8_t *addr, 
	uint32_t read_bytes, uint32_t zero_bytes, bool writable)
{
	struct vm_page *p;

	p = malloc (sizeof (struct vm_page));


	if(p == NULL)
		return false;

	//printf("fileff : 0x%x, add : 0x%x, ofs : 0x%x\n", file_get_inode(file), addr, ofs);

//	printf("set addr : 0x%x\n", addr);

	p->file = file;
	p->offset = ofs;
	p->addr = addr;
	p->read_bytes = read_bytes;
	p->zero_bytes = zero_bytes;
	p->writable = writable;

	p->loaded = false;
	p->type = FILE;

	p->kpage = NULL;

	struct hash_elem *success = hash_insert(&thread_current()->page_table, &p->hash_elem);

	if(success == NULL){

		//printf("page : success %d - 0x%x\n", p->type, p->addr);
		return true;
	}

	return false;
}

bool get_zero_page_table(uint8_t *addr, bool writable)
{
	struct vm_page *p;

	p = malloc (sizeof (struct vm_page));

	if(p == NULL)
		return false;

	//printf("ZERO page --- add : 0x%x\n",addr);


	p->addr = addr;
	p->writable = writable;

	p->loaded = false;
	p->type = ZERO;

	p->kpage = NULL;

	struct hash_elem *success = hash_insert(&thread_current()->page_table, &p->hash_elem);

	if(success == NULL){

		//printf("page : success %d - 0x%x\n", p->type, p->addr);
		return true;
	}

	return false;
}
/*
bool get_swap_page_table(uint8_t *addr, bool writable)
{
	struct vm_page *p;

	p = malloc (sizeof (struct vm_page));

	if(p == NULL)
		return false;

	p->addr = addr;
	p->writable = writable;

	p->loaded = true;
	p->type = SWAP;

	struct hash_elem *success = hash_insert(&thread_current()->page_table, &p->hash_elem);

	if(success == NULL){

		//printf("page : success %d - 0x%x\n", p->type, p->addr);
		return true;
	}

	return false;
}*/

bool load_page(struct vm_page *p)
{

	lock_acquire(&page_lock);

	if(p->loaded)
		return true;

	switch(p->type)
	{
		case MMAP:
		case FILE:
		{
			if(p->kpage == NULL)
				p->kpage = get_frame(p, PAL_USER, 0);
			//uint8_t *addr = get_frame(p, PAL_USER, 0);

			//printf("get_addr : 0x%x\n", addr);

			//printf("p->FILE : 0x%x\n", p->addr);

			//if(addr == NULL){
			if(p->kpage == NULL){
				printf("that's NULL!\n");
				return false;
			}

			//printf("read_bytes : 0x%x\n", p->read_bytes);

			if(p->read_bytes > 0)
			{
				if(!page_file_load(p, p->kpage)){
					printf("that's unload!\n");
					return false;
				}
			}

			/* process.c   install_page */
			//if(!(pagedir_get_page (thread_current()->pagedir, p->addr) == NULL
		    //      && pagedir_set_page (thread_current()->pagedir, p->addr, addr, p->writable)))
			if(!install_page(p->addr, p->kpage, p->writable))
			{
				free_frame(p->kpage);

				printf("that's fault!\n");
				return false;
			}
							
			break;
		}
		case ZERO:
		{
			//printf("ZERO\n");

			//uint8_t *addr = get_frame(p, (PAL_USER | PAL_ZERO), 0);
			
			if(p->kpage == NULL)
				p->kpage = get_frame(p, PAL_ZERO, 0);

			//uint8_t *addr = get_frame(p, PAL_ZERO, 0);

			//printf("p->ZERO : 0x%x\n", p->addr);

			

			//if(addr == NULL){
			if(p->kpage == NULL){
				printf("that's NULL!\n");
				return false;
			}

			memset(p->kpage, 0, PGSIZE);			

			//if(!(pagedir_get_page (thread_current()->pagedir, p->addr) == NULL
		    //      && pagedir_set_page (thread_current()->pagedir, p->addr, addr, p->writable)))
			
			if(!install_page(p->addr, p->kpage, p->writable))
			{
				free_frame(p->kpage);

				printf("that's fault!\n");
				return false;
			}

			//printf("end of install zero page\n");
							
			break;
		}
		case SWAP:
		{
		
			// if(addr == NULL){
			// 	printf("that's NULL!\n");
			// 	return false;
			// }			

			// //if(!(pagedir_get_page (thread_current()->pagedir, p->addr) == NULL
		 //    //      && pagedir_set_page (thread_current()->pagedir, p->addr, addr, p->writable)))
			// if(!install_page(p->addr, addr, p->writable))
			// {
			// 	free_frame(addr);

			// 	printf("that's fault!\n");
			// 	return false;
			// }


			//printf("--load to swap\n");

			if(p->kpage == NULL)
			{
				p->kpage = get_frame(p, PAL_USER, 0);
				if(p->kpage == NULL)
					return false;

				//printf("SWAP : 0x%x\n", p->kpage);

				if(!install_page(p->addr, p->kpage, p->writable))
				{
					free_frame(p->kpage);
					return false;
				}
			}

			//printf("index : %d, kpage 0x%x\n", p->swap_index, p->kpage);

			swap_load(p->swap_index, p->kpage);
			swap_free(p->swap_index);

			//printf("swap load complete, 0x%x, 0x%x\n", p->addr, p->kpage);
							
			break;
		}
		default:
			break;
	}

	p->loaded = true;


	/* set dirty bit */

	struct vm_frame *fr = find_frame(p->kpage);
	struct thread * cur = get_thread(fr->tid);
/*
	if(fr == NULL)
		printf("fr asdfasdfasdf\n");
	if(cur == NULL)
		printf("th asdfasdfasdf\n");

	printf("frame tid : %d\n", fr->tid);*/


	//pagedir_set_dirty(cur->pagedir, p->addr, false);
	pagedir_set_dirty(thread_current()->pagedir, p->addr, false);

	//printf("can't set dirty bit\n");

	lock_release(&page_lock);

	return true;

	// uint8_t *kpage = get_frame(p, PAL_USER);

	// if(kpage == NULL)
	// 	return false;

	// return true;

}

bool page_file_load(struct vm_page *p, uint8_t *addr)
{
	lock_acquire(&syscall_lock);

	//printf("fileld : 0x%x\n", file_get_inode(p->file));

	size_t re = file_read_at(p->file, addr, p->read_bytes, p->offset);
	//size_t re = file_read(p->file, addr, p->read_bytes);

	//printf("addr : 0x%x, read_bytes : 0x%x --- 0x%x, offset : 0x%x\n", addr, p->read_bytes, re, p->offset);
	
	// if(p->read_bytes == file_read_at(p->file, addr, p->read_bytes, p->offset))
	// {
	if(p->read_bytes == re)
	{
		lock_release(&syscall_lock);
		memset(addr + p->read_bytes, 0, p->zero_bytes);
	} else
	{
		lock_release(&syscall_lock);
		free_frame(addr);
		return false;
	}

	return true;
}


unsigned page_hash (const struct hash_elem *p_, void *aux UNUSED)
{
	const struct vm_page *p = hash_entry (p_, struct vm_page, hash_elem);

	return hash_bytes (&p->addr, sizeof p->addr);
}

bool page_less (const struct hash_elem *a_, const struct hash_elem *b_,
		void *aux UNUSED)
{
	const struct vm_page *a = hash_entry (a_, struct vm_page, hash_elem);
	const struct vm_page *b = hash_entry (b_, struct vm_page, hash_elem);

	return a->addr < b->addr;
}

struct vm_page *page_lookup (const void *address)
{
	struct vm_page p;
	struct hash_elem *e;

	p.addr = (void *) address;

	//printf("add : 0x%x\n", address);

	e = hash_find (&thread_current()->page_table, &p.hash_elem);

	return e != NULL ? hash_entry (e, struct vm_page, hash_elem) : NULL;
}

struct vm_page *mmap_lookup (mapid_t mapping)
{
	struct hash_iterator i;

	hash_first(&i, &thread_current()->page_table);

	while(hash_next(&i))
	{
		struct vm_page *p = hash_entry (hash_cur(&i), struct vm_page, hash_elem);

		if(p->mmapid == mapping)
			return p;
	}

	return NULL;

	//return e != NULL ? hash_entry (e, struct vm_page, hash_elem) : NULL;
}


void page_action_func (struct hash_elem *elem, void *aux UNUSED)
{
	struct vm_page *p = hash_entry(elem, struct vm_page, hash_elem);

	free(p);
}