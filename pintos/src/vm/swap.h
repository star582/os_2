#ifndef VM_SWAP_H
#define VM_SWAP_H

#include "devices/block.h"
#include "threads/synch.h"
#include "lib/kernel/bitmap.h"

struct block *swap_block;

struct lock swap_lock;

struct bitmap *swap_map;

void swap_init(void);

void swap_load (size_t, void *);

size_t swap_get (void *);

void swap_free (size_t);

#endif /* vm/swap.h */