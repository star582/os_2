#ifndef VM_PAGE_H
#define VM_PAGE_H

#include "lib/kernel/hash.h"
#include "filesys/file.h"
#include "userprog/syscall.h"
#include "threads/synch.h"


enum vm_page_type
{
	FILE = 0,
	SWAP = 1,	
	MMAP = 2,
	ZERO = 3
};

//From document

struct vm_page
{
	void *addr;	/* Virtual address */
	void *kpage; /* physical address */

	enum vm_page_type type;

	bool loaded;
	bool writable;

	struct file *file;
	off_t offset;
	size_t read_bytes;
	size_t zero_bytes;

	size_t swap_index;

	mapid_t mmapid;


	struct hash_elem hash_elem;	// hash_elem
	
};

struct lock page_lock;

/* project 3-1 add */

void page_init(void);

void free_page (struct vm_page *);

bool get_file_page_table(struct file *, off_t, uint8_t *, uint32_t,
	uint32_t, bool);
bool get_zero_page_table(uint8_t *, bool);
//bool load_page(void *upage, void *kpage, bool writable);
bool load_page(struct vm_page *);


/* in document */
unsigned page_hash (const struct hash_elem *, void *);
bool page_less (const struct hash_elem *, const struct hash_elem *,
		void *);
struct vm_page *page_lookup (const void *);
struct vm_page *mmap_lookup (mapid_t);
void page_action_func (struct hash_elem *, void *);

bool page_file_load(struct vm_page *,uint8_t *);

#endif /* VM_PAGE_H */