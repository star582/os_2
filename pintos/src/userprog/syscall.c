#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "devices/shutdown.h"

/* project 2 */

#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/tss.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "threads/flags.h"
#include "threads/init.h"
#include "threads/palloc.h"
#include "threads/vaddr.h"
#include "devices/input.h"

/* project 3-1 */

#ifdef VM
#include "vm/page.h"
#include "vm/frame.h"
#include "vm/swap.h"
#endif

#include "threads/malloc.h"


static void syscall_handler (struct intr_frame *);
static int get_user (const uint8_t *);
static bool put_user (uint8_t *, uint8_t );

static void * stackpointer;


/* Reads a byte at user virtual address UADDR.
   UADDR must be below PHYS_BASE.
   Returns the byte value if successful, -1 is a segault occurred. */

static int
get_user (const uint8_t *uaddr)
{
  int result;
  asm ("movl $1f, %0; movzbl %1, %0; 1:"
  : "=&a" (result) : "m" (*uaddr));
  return result;
}

/* Writes BYTE to user address UDST.
UDST must be below PHYS_BASE.
Returns true if successful, false if a segfault occurred. */
static bool
put_user (uint8_t *udst, uint8_t byte)
{
  int error_code;
  asm ("movl $1f, %0; movb %b2, %1; 1:"
  : "=&a" (error_code), "=m" (*udst) : "q" (byte));
  return error_code != -1;
}

/* functino in project2 */




void get_argument_byte(struct intr_frame *f, unsigned *args, int count);

int process_file_push(struct file *file);
struct process_filesys * get_process_file(int fd);


void valid_address(void *address)
{
  /* user prog address in 0x08048000 - 0xc0000000 */
  unsigned int point = (unsigned int) address;

  //printf("PGSIZE : 0x%x\n", PGSIZE);
  //printf("add : 0x%x\n", point);

  if(!(0x08048000 < point) || !((unsigned) PHYS_BASE > point)){
   //printf("-----yes yes----- 0x%x 0x%x\n", point, &point);
    return exit(-1);
  }
}

void
syscall_init (void) 
{
  lock_init(&syscall_lock); // for filesys lock.
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");

}

static void
syscall_handler (struct intr_frame *f UNUSED) 
{
  unsigned args[3];    // system_argument list
  
  int syscall_name;

  //printf("fork you : 0x%x\n", f->esp);
  valid_address(f->esp);  //  valid syscall check

  syscall_name = * (int *)f->esp;

  //printf("sys call : 0x%x, esp : 0x%x\n", syscall_name, f->esp);

  printf("", * (int *) f->esp, f->esp); // needed. IDK

  stackpointer = f->esp;
  
  switch(syscall_name){

    case SYS_HALT:  //   0x0
    {
      halt();
      break;
    }
    case SYS_EXIT:  //  0x1
    {
      get_argument_byte(f, args, 1);

      //printf("exit : ?? %d ??\n", args[0]);
      exit(args[0]);
      break;
    }
    case SYS_EXEC:  //  0x2
    {
      get_argument_byte(f, args, 1);

      //printf("args : %s\n", args[0]);

      f->eax = (uint32_t) exec((const char *) args[0]);

      //printf("do\n");


      break;
    }
    case SYS_WAIT:  //  0x3
    {
      get_argument_byte(f, args, 1);

      f->eax = (uint32_t) wait(args[0]);
      break;
    }
    case SYS_CREATE:  // 0x4
    {
      get_argument_byte(f, args, 2);

      f->eax = create((const char *)args[0], (unsigned) args[1]);

      break;
    }

    case SYS_REMOVE:  // 0x5
    {
      get_argument_byte(f, args, 1);

      f->eax = remove((char *) args[0]);
      break;
    }
    case SYS_OPEN:  // 0x6
    {
      get_argument_byte(f, args, 1);

      f->eax = open((char *) args[0]);


      //printf("open : 0x%x\n", f->eax);
      break;
    }
    case SYS_FILESIZE:  // 0x7
    {
      get_argument_byte(f, args, 1);
      f->eax = filesize(args[0]);
      break;
    }
    case SYS_READ:  // 0x8
    {
      get_argument_byte(f, args, 3);

      f->eax = read(args[0], (void *) args[1], (unsigned) args[2]);
      break;
    }
    case SYS_WRITE:   // 0x9
    {
      get_argument_byte(f, args, 3);

      f->eax = write(args[0], (void *)args[1], (unsigned) args[2]);
      break;
    }
    case SYS_SEEK:  // 0xa
    {
      get_argument_byte(f, args, 2);

      seek(args[0], (unsigned) args[1]);
      break;
    }
    case SYS_TELL:  // 0xb
    {
      get_argument_byte(f, args, 1);

      f->eax = tell(args[0]);
      break;
    }
    case SYS_CLOSE:  // 0xc
    {
      get_argument_byte(f, args, 1);

      close(args[0]);

      break;
    }
    case SYS_MMAP:  // 0xd
    {
      get_argument_byte(f, args, 2);

      f->eax = mmap(args[0], (void *) args[1]);
      break;
    }
    case SYS_MUNMAP:  //0xe
    {
      get_argument_byte(f, args, 1);

      munmap(args[0]);

      break;
    }

    default:
    {
      printf("--system call!\n");

    }
  }

  //printf("f->eax : 0x%x\n", &f->eax);
  //printf("f->esp : 0x%x\n", &f->esp);
}


void get_argument_byte(struct intr_frame *f, unsigned *args, int count)
{
  int i, j;
  char *ptr;

  unsigned temp = 0;
  unsigned result = 0;

  for(i = 0; i < count; i++)
  {
    result = 0;
    temp = 0;

    ptr = f->esp + 4*(i+1);    

    for(j = 0; j < 4; j++){
      valid_address(ptr + j);
      temp = get_user((const uint8_t *)ptr + j);
      //printf("%u - ", temp == (unsigned)-1);
      if((int)temp == -1){
        //printf("it's me\n");
        exit(-1);
    }
      else
        result += (temp << (8*j));
    }


    args[i] = result;
    //printf("count");
  }

  //printf("\n");
  //printf("end of the game\n");
}

/* open filesys and add in process_file list */
int process_file_push(struct file *file_)
{
  struct process_filesys *process_file;

  process_file = malloc(sizeof (struct process_filesys));

  if(process_file == NULL){
    printf("---- ERROR : can't get page!\n");
    free(process_file);
    return -1;
  }

  //printf("readched\n");

  process_file->file = file_;

  //printf("tid : %u\n", thread_current()->tid);

  if(list_empty(&thread_current()->file_list)){
    //printf("empty\n");
    process_file->fd = 3;
  } else{
    process_file->fd = list_entry(list_back(&thread_current()->file_list),
      struct process_filesys, elem)->fd + 1;
  }

  //printf("readched2\n");

  list_push_back(&thread_current()->file_list, &process_file->elem);

  //printf("list_size : %d\n", list_size(&thread_current()->file_list));

  //printf("file fd : %x\n", process_file->fd);
  return process_file->fd;
}

static mapid_t mmapid = 0;


struct process_filesys *
get_process_file(int fd)
{
  struct list_elem *e;
  struct process_filesys *process_file;

  for (e = list_begin (&thread_current()->file_list); e != list_end (&thread_current()->file_list);
       e = list_next (e))
    {
      process_file = list_entry (e, struct process_filesys, elem);
      if (process_file->fd == fd){
        return process_file;
      }
    }
  return NULL;
}

void halt (void) 
{
  shutdown_power_off();
}

void exit (int status){

  //printf("%d\n", status);
  
  struct thread * parent = get_thread(thread_current()->parent_tid);

  if(parent != NULL){
    struct child_process *child = get_child_process(thread_current()->tid, &parent->child_list);
    
    if(child != NULL){

      //printf("HI \n");

      //lock_release(&syscall_lock);

      //printf("child->status : %d\n", child->status);

      child->exited = true;
      child->status = status;
      /*
      if(parent != NULL){

        if(child->wait)
        {

          enum intr_level old_level = intr_disable();

          thread_unblock(parent);

          intr_set_level(old_level);
        }

      }*/

        //printf("%d\n", mmapid);

        if(mmapid > 0)
          munmap(mmapid);

      printf("%s: exit(%d)\n", (char *)&thread_current()->name, status);
    }
  }

  thread_exit();

}

pid_t exec(const char *cmd_line){

  valid_address((void *)cmd_line);

  if(get_user((void *)cmd_line) == -1)
  {
    exit(-1);
    return -1;
  }

  //printf("valid : %s\n", cmd_line);

  return process_execute(cmd_line);

}

int wait (pid_t pid)
{

//printf("%d : wait\n", pid);

  return process_wait(pid);
}

bool create (const char *file, unsigned initial_size) // 0x4
{
  bool result;

  //printf("file : 0x%x, %d\n", file, initial_size);

  valid_address((void *)file);


  lock_acquire(&syscall_lock);

  //printf("i'm hear\n");

  // if(file + initial_size - 1 >= PHYS_BASE ||
  //   get_user(file + initial_size - 1) == -1){
  //   exit(-1);
  //   return -1;
  // }

  // if(put_user((void *)(file), initial_size) == -1){
  //   exit(-1);
  //   return -1;
  // }

  if(get_user(file) == -1)
  {
    exit(-1);
    return -1;
  }


  result = filesys_create(file, initial_size);

  //printf("i'm hear\n");

  lock_release(&syscall_lock);

  //printf("i'm hear\n");

  return result;
}


bool remove (const char *file)
{
  valid_address((void *)file);

  bool result;

  lock_acquire(&syscall_lock);

  result = filesys_remove(file);

  lock_release(&syscall_lock);

  return result;
}

int open (const char *file) // 0x6
{
  valid_address((void *)file);

  if(get_user((void *)file) == -1)
  {
    exit(-1);
    return -1;
  }

  struct file *f;

  //int face = 0;

  lock_acquire(&syscall_lock);

  //printf("i'll open you\n");

  f = filesys_open(file);

  //printf("hohoho\n");

  lock_release(&syscall_lock);

  //printf("come in %d\n", list_size(&thread_current()->file_list));

  if(!f)
  {
    //printf("you did it-------------\n");
    return -1;
  } else
  {

    //printf("hohohoho\n");

    return process_file_push(f);
  }

  //printf("hoho\n");


}


int filesize (int fd)
{
  lock_acquire(&syscall_lock);
  struct process_filesys * process_file = get_process_file(fd);
  if (process_file == NULL){
    lock_release(&syscall_lock);
    return -1;
  }

  int size = file_length(process_file->file);
  lock_release(&syscall_lock);
  return size;
}


int read (int fd, void *buffer, unsigned size)
{
  //printf("start read\n");
  valid_address(buffer + size);
  valid_address(buffer);

  //printf("read : %d, buffer : 0x%x, size : %u\n", fd, buffer, size);
/*
  if(get_user((void *)(buffer + size - 1)) == -1 &&
    get_user((void *)(buffer)) == -1)
  {
    //printf("read OMG\n");
    exit(-1);
    return -1;
  }
*/

  // struct vm_page *p = page_lookup(pg_round_down(buffer));
  // bool success = true;

  // if (p == NULL)
  // {
  //   success = grow_stack(pg_round_down(buffer));
  // }

  // if(!success){
  //   printf("failed grow_stack in read\n");
  // }
/*
  if(!put_user((uint8_t *) buffer, size))
  {
    //printf("failed in put_user\n");
    exit(-1);
    return -1;
  }
*/
  // if(!success){
  //   exit(-1);
  //   return -1;
  // }

  unsigned i;
  uint8_t* tmp_buffer;
  struct process_filesys * process_file;


  //printf("boing\n");

  if (fd == 0){
    tmp_buffer = (uint8_t *) buffer;

    lock_acquire(&syscall_lock);

    for (i = 0; i < size; i++)
    {
      tmp_buffer[i] = input_getc();
    }

    lock_release(&syscall_lock);

    return size;
  } else if(fd == 1)
    return -1;


  lock_acquire(&syscall_lock);

  process_file = get_process_file(fd);

  
  if (process_file == NULL || process_file->file == NULL){
    lock_release(&syscall_lock);
    return -1;
  }

  lock_release(&syscall_lock);


  int reads = 0;
  bool success = true;

  void *buf_ofs = (void *)buffer;

  while(size > 0)
  {
    size_t ofs = (size_t) pg_round_down(buf_ofs);
    struct vm_page *p = page_lookup((void *)ofs);

    //printf("ofs : 0x%x\n", ofs);

    //printf("p->addr : 0x%x, type : %d\n", p->addr, p->type);
    //printf("p->file : 0x%x\n", file_get_inode(p->file));

    if(p == NULL)
    {
      valid_address((void *)ofs);
      if(ofs >= (stackpointer - 32) && (PHYS_BASE - ofs) <= 0x800000)
      {
        //printf("grrroowowowoow\n");
        success = grow_stack((void *)ofs);
        p = page_lookup((void *)ofs);
      } else
      {
        exit(-1);
        return -1;
      }
    }

    if(!success)
    {
      exit(-1);
      return -1;
    }

    if(!p -> loaded)
      load_page(p);

    /*if(!put_user((uint8_t *) buffer, size))
    {
      printf("failed in put_user\n");
      exit(-1);
      return -1;
    }*/



    ofs = (size_t)buf_ofs - ofs;

    size_t read_bytes = PGSIZE >= ofs + size? size : size - (ofs + size - PGSIZE);

    
    //printf("buf_ofs : 0x%x, ofs : 0x%x, read : 0x%x, size : 0x%x\n", buf_ofs, ofs, read_bytes, size);
    //printf("ad in 0x%x\n", * (int *)(buf_ofs + read_bytes));

    lock_acquire(&syscall_lock);

    ASSERT(p->loaded);

    reads += file_read(process_file->file, buf_ofs, read_bytes);
    lock_release(&syscall_lock);

    //printf("size : %d, read : %d \n", size, read_bytes);
    size -= read_bytes;
    //printf("size : %d, read : %d \n", size, read_bytes);
    buf_ofs += read_bytes;

    //printf("reads : %d\n", reads);
  }

  //printf("end of read\n");

  

  return reads;
}



int write (int fd, const void *buffer, unsigned size) // 0x9
{

  //printf("write start\n");

  valid_address((void *)buffer + size - 1);
  valid_address((void *)buffer);

  //printf("out : 0x%x  -  %d\n", buffer, size);

  /*struct vm_page *p = page_lookup(pg_round_down(buffer));

  if (p != NULL)
    //printf("LOOOOOOOOOOOOL\n");*/

  if(get_user((void *)(buffer)) == -1)
  {
    //printf("not user address\n");
    exit(-1);
    return -1;
  }

  //printf("-outaaa : %d\n", fd);

  if(fd == 1){
    lock_acquire(&syscall_lock);
    putbuf(buffer, size);
    lock_release(&syscall_lock);

    //printf("buffer : 0x%x, size : %d\n", buffer, size);

    return size;
  } else if(fd == 0 || fd == 2){    //stdout or stdError

    //printf("std : %d\n", fd);
    exit(-1);
    return -1;
  }

  //printf("-out\n");

  lock_acquire(&syscall_lock);

  int writes = 0;
  struct process_filesys * process_file = get_process_file(fd);
  
  if(process_file == NULL)
  {
    lock_release(&syscall_lock);
    return writes;
  }

  lock_release(&syscall_lock);

  void *buf_ofs = (void *)buffer;

  while (size > 0)
  {
    size_t ofs = (size_t)pg_round_down(buf_ofs);
    struct vm_page *p = page_lookup((void *)ofs);

    bool success = true;

    if(p == NULL)
    {
      valid_address(ofs);
      if(ofs >= (stackpointer - 32) && (PHYS_BASE - ofs) <= 0x800000)
      {
        //printf("grrroowowowoow\n");
        success = grow_stack((void *)ofs);
        p = page_lookup((void *)ofs);
      } else
      {
        exit(-1);
        return -1;
      }
    }

    if(!success)
    {
      exit(-1);
      return -1;
    }

    if(!p->loaded)
      load_page(p);

    ofs = (size_t)buf_ofs - ofs;

    size_t write_bytes = PGSIZE >= ofs + size? size : size - (ofs + size - PGSIZE);

    lock_acquire(&syscall_lock);

    writes += file_write(process_file->file, buf_ofs, write_bytes);

    lock_release(&syscall_lock);

    //printf("buf_ofs : 0x%x, write : 0x%x, size : 0x%x\n", buf_ofs, write_bytes, size);

    size -= write_bytes;
    buf_ofs += write_bytes;
  }

  //printf("--out\n");

  return writes;
}


void seek (int fd, unsigned position)
{
  lock_acquire(&syscall_lock);
  struct process_filesys * process_file = get_process_file(fd);
  if (process_file == NULL){
    lock_release(&syscall_lock);
    return;
  }

  file_seek(process_file->file, position);
  lock_release(&syscall_lock);
}

unsigned tell (int fd)
{
  lock_acquire(&syscall_lock);
  struct process_filesys * process_file = get_process_file(fd);
  if (process_file == NULL){
    lock_release(&syscall_lock);
    return -1;
  }

  off_t tells = file_tell(process_file->file);
  lock_release(&syscall_lock);
  return tells;
}


void close (int fd)
{
  struct list_elem *e;
  struct process_filesys *process_file;

  for (e = list_begin (&thread_current()->file_list); e != list_end (&thread_current()->file_list);
       e = list_next (e))
    {
      process_file = list_entry (e, struct process_filesys, elem);
      if (process_file->fd == fd){

        lock_acquire(&syscall_lock);

        file_close(process_file->file);
        //printf("remove");
        list_remove(e);
        free(process_file);

        lock_release(&syscall_lock);
        return;
      }
    }
}




mapid_t mmap (int fd, void *addr){

  size_t size;

  size = filesize(fd);
  lock_acquire(&syscall_lock);
  struct process_filesys * process_file = get_process_file(fd);
  lock_release(&syscall_lock);

  //printf("hey fork you, fd : %d, addr : 0x%x\n", fd, addr);

  //printf("pg_ofs : 0x%x\n", pg_ofs(process_file->file));

  if(size <= 0 || process_file == NULL)
    return -1;

  //valid_address(addr);

  if(!(0x08048000 < addr) || !((unsigned) PHYS_BASE > addr)){
   //printf("-----yes yes----- 0x%x 0x%x\n", point, &point);
    return -1;
  }

  //printf("stack : 0x%x\n", stackpointer);

  if(fd == 0 || fd == 1)
    return -1;

  void * tmp_addr = addr;
  size_t ofs = 0;

  //printf("hey fork you\n");

  struct file *f = file_reopen(process_file->file);

  //printf("successful file reopen\n");

  while (size > 0)
  {
    size_t read_bytes, zero_bytes;

    read_bytes = size >= PGSIZE ? PGSIZE : size;
    zero_bytes = PGSIZE - read_bytes;

    struct vm_page *p = page_lookup(tmp_addr);

    if(p != NULL)
      return -1;

    //printf("size : %d, read : 0x%x, addr : 0x%x\n", size, read_bytes, tmp_addr);

    if(!get_file_page_table(f, ofs, tmp_addr, read_bytes, zero_bytes, true))
    {
      //printf("can't make page in mmap\n");
      munmap(mmapid-1);
      return -1;
    }

    //printf("OMG IM ALIVE\n");

    p = page_lookup(tmp_addr);
    p->type = MMAP;
    p->mmapid = ++mmapid;

    ofs += read_bytes;
    size -= read_bytes;
    tmp_addr += read_bytes; 

    //printf("To the NEXT size : %d\n", size);
    //printf("p->mmapid : %d\n", p->mmapid);

  }

  //file_close(f);

  //printf("return mmapid : %d, \n", mmapid);

  return mmapid;

}

void munmap (mapid_t mapping)
{
  struct vm_page *p = mmap_lookup(mapping);
  struct thread *cur = thread_current();

  //printf("unmap : %d\n", mapping);

  if(p == NULL)
    return;

  //printf("p not NULL\n");

  struct file *f = file_reopen(p->file);

  //printf("file is? : %d\n", f);

  if(pagedir_is_dirty(cur->pagedir, p->addr))
  {
    file_write_at(p->file, p->addr, p->read_bytes, p->offset);
  }

  //pagedir_clear_page(cur->pagedir, p->addr);
  free_page(p);

  file_close(f);


  return;
}