#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H


#include "userprog/process.h"
#include "threads/synch.h"

typedef int pid_t;

/* project 3-2 will be add */
typedef int mapid_t;

void syscall_init (void);
void valid_address(void *address);

void halt(void);
void exit (int );
pid_t exec(const char *);
int wait (pid_t pid);
bool create (const char *file, unsigned initial_size);
bool remove (const char *file);
int open (const char *file);
int filesize (int fd) ;
int read (int fd, void *buffer, unsigned size);
int write (int fd, const void *buffer, unsigned size);
void seek (int fd, unsigned position);
unsigned tell (int fd);
void close (int fd);

/* Project 3-2 will modify */
mapid_t mmap (int fd, void *addr);
void munmap (mapid_t);

struct lock syscall_lock;

#endif /* userprog/syscall.h */
