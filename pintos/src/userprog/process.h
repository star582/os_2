#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"

tid_t process_execute (const char *file_name);
bool check_process(void *);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);
void argument_passing(char **, int, void **);
int make_child(int);

bool install_page (void *upage, void *kpage, bool writable);

#endif /* userprog/process.h */
